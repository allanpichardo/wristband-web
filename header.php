<?php

function getScriptName() {
    return basename($_SERVER["SCRIPT_FILENAME"], '.php');
}

function setSelectedClass($desired) {
    $class = $desired == getScriptName() ? "class='core-selected'" : "";
    echo $class;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Wristband NYC - Who's playing tonight?</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name='apple-mobile-web-app-capable' content='yes'>
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="description" content="Wristband NYC is a simple live music discovery tool to help you find and screen concerts happening in New York City. If you like a band, upvote it, or leave a quick comment about how dreamy the drummer was. It's like Yelp for live music, only there are no accounts. Simplicity is the key.">

        <link href='http://fonts.googleapis.com/css?family=Raleway:500,400' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href='wristband.css'>

        <script src="bower_components/webcomponentsjs/webcomponents.min.js"></script>
        <script src="wristband.js"></script>

        <link rel="import" href="elements/wristband-shows.html">
        <link rel="import" href="bower_components/core-scaffold/core-scaffold.html">
        <link rel="import" href="bower_components/core-header-panel/core-header-panel.html">
        <link rel="import" href="bower_components/core-scroll-header-panel/core-scroll-header-panel.html">
        <link rel="import" href="bower_components/core-menu/core-menu.html">
        <link rel="import" href="bower_components/core-item/core-item.html">
        <link rel="import" href="bower_components/core-icon-button/core-icon-button.html">
        <link rel="import" href="bower_components/core-toolbar/core-toolbar.html">
        <link rel="import" href="bower_components/core-menu/core-submenu.html">
        <link rel="import" href="bower_components/paper-progress/paper-progress.html">
        <link rel="import" href="bower_components/core-menu/core-submenu.html">
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-30075056-2', 'auto');
            ga('send', 'pageview');

        </script>
    </head>
    <body fullbleed layout vertical>
    <core-drawer-panel transition id="coredrawerpanel" touch-action fit>
        <section drawer>
            <core-header-panel style="overflow-y: hidden" fit mode="seamed">
                <core-toolbar id="core_toolbar"></core-toolbar>
                <core-menu valueattr="label" id="core_menu" theme="core-light-theme">
                    <!--<core-submenu label="Cities" selected="0">
                        <core-item id="nycItem" label="New York City" horizontal center layout><a href="index.php"></a></core-item>
                        <core-item id="miamiItem" label="Miami" horizontal center layout><a href="miami.php"></a></core-item>
                        <core-item id="seattleItem" label="Seattle" horizontal center layout><a href="seattle.php"></a></core-item>
                    </core-submenu>-->

                    <core-item id="faqItem1" label="FAQs" horizontal layout><a href="faq.php"></a></core-item>
                    <core-item id="aboutItem" label="About" horizontal center layout><a href="about.php"></a></core-item>

                </core-menu>
                <footer id="footer" class="bottom">
                    &copy;2015 <a href="mailto:imaginarymercenary@gmail.com">Allan Pichardo</a>
                </footer>
            </core-header-panel>
        </section>