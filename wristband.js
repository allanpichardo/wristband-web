/* 
 * Copyright 2015 Allan Pichardo<allan.pichardo@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


document.addEventListener('polymer-ready', function () {
    setHeader();
});

function setHeader() {
    var date = new Date();
    var header = document.getElementById('titleBar');

    header.innerHTML += date.toDateString();
}
function scrollHandler(event) {
    var scroller = event.detail.target;
    console.log(scroller.scrollTop);
}