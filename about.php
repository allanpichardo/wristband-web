<?php include_once 'header.php'; ?>
<section main>
    <core-header-panel id="mainHeaderPanel" fit mode="seamed">
        <core-toolbar id="title">
            <core-icon-button id="menuButton" icon="menu" core-drawer-toggle/></core-icon-button> 
            <span flex><a href="index.php">Wristband NYC</a></span><a href="http://www.songkick.com" target="_blank"><img src="img/songkick.png" alt="concerts by songkick"></a>
        </core-toolbar>
        <div class="content" id="content" fit>
            <div id="expo">
                <span style="text-align: center"><h2>About Wristband</h2></span>
            
            <p>Wristband is a simple live music discovery tool created by Allan Pichardo to find concerts happening in New York City. If you like a band, upvote it, or leave a quick comment about how dreamy the drummer was. It's like Yelp for live music, only there are no accounts. Simplicity is the key.</p>

            <span style="text-align: center"><h2>Contact</h2></span>
            <p>Questions? Comments? Let me know what you think. <a href="mailto:imaginarymercenary@gmail.com">imaginarymercenary@gmail.com</a></p>
            </div>
        </div>
    </core-header-panel>
</section>
<?php include_once 'footer.php'; ?>