<?php
include_once 'header.php';
?>
<section main>
    <core-header-panel id="mainHeaderPanel" fit mode="seamed">
        <core-toolbar id="title">
            <core-icon-button id="menuButton" icon="menu" core-drawer-toggle/></core-icon-button> 
            <span id="titleBar" flex>NYC: </span><a href="http://www.songkick.com" target="_blank"><img src="img/songkick.png" alt="concerts by songkick"></a>
        </core-toolbar>
        <div class="content" id="content" fit>
            <paper-progress indeterminate="true" id="mainProgress" class="bottom"></paper-progress>
            <wristband-shows latitude="40.8241729" longitude="-73.9435673" progressbar="mainProgress" headerpanel="mainHeaderPanel"></wristband-shows>
        </div>
    </core-header-panel>
</section>
<?php
include_once 'footer.php';
?>