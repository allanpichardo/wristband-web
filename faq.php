<?php include_once 'header.php'; ?>
<section main>
    <core-header-panel id="mainHeaderPanel" fit mode="seamed">
        <core-toolbar id="title">
            <core-icon-button id="menuButton" icon="menu" core-drawer-toggle/></core-icon-button> 
            <span flex><a href="index.php">Wristband NYC</a></span><a href="http://www.songkick.com" target="_blank"><img src="img/songkick.png" alt="concerts by songkick"></a>
        </core-toolbar>
        <div class="content" id="content" fit>
            <div id="expo">
                <span style="text-align: center"><h2>FAQs</h2></span>
            <a href="#why"><h3>Why?</h3></a>

            <p>There's nothing I love more than discovering new music. Checking out a random club or party and checking out some live music. Sometimes, I come away with that great feeling that I've seen something awesome. Sometimes, not so much. In a city like NYC, even on a slow night, you still have a staggering amount of choices when it comes to live music. I built Wristband because I wanted an easy-to-use tool for discovering new music that was free of hype. Just press play and let the music speak for itself.</p>

            <a href="#howitworks"><h3>How does it work?</h3></a>

            <p>Wristband simply pairs the concerts listed on Songkick and matches those to Bandcamp profiles. Lucky for all of us, the way to add your shows to Bandcamp is to list them on Songkick. You'll also notice that the date cannot be changed. By design, I wanted Wristband to be a tool for spontaneity. Use it to find something good tonight.</p>

            <a href="#canijoin"><h3>Can my band be on it?</h3></a>

            <p>How do you know that it's not already there? Simply have a Bandcamp profile and list your concerts on Songkick. Next time you play NYC, you'll be on Wristband.</p>
            </div>
        </div>
    </core-header-panel>
</section>
<?php include_once 'footer.php'; ?>